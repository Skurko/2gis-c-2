﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lecture2
{
    class Program
    {
        //Ввести с консои N чисел, найти их сумму
        static void Task1() 
        {
            var inputStr = Console.ReadLine();
            var numbers = inputStr.Split(new char[] {' '});
            double result = 0;
            foreach (var num in numbers)
            {
                result += Convert.ToDouble(num);
            }
            Console.WriteLine("Введено {0} чисел", numbers.Length);
            Console.WriteLine("Их сумма: " + result);
            Console.ReadKey();
        }

        //Возвести число X в степень N
        static void Task2()
        {
            var x = Convert.ToDouble(Console.ReadLine());
            var n = Convert.ToInt32(Console.ReadLine());
            var res = x;
            for (var i = 2; i <= n; i++)
            {
                res *= x;
            }
            Console.WriteLine("X^N = " + res);
            Console.ReadKey();
        }

        //Калькулятор. Вводится левый операнд, оператор, правый оператор. Выводится результат. Предусмотреть обработку ошибок. 
        static void Task3()
        {
            char[] operations = new char[] {'+', '-', '*', '/', '^'};

            var inputStr = Console.ReadLine();
            var elements = inputStr.Split(new char[] { ' ' });

            if (elements.Length != 3)
            {
                if (elements.Length > 3)
                {
                    throw new Exception("Too many input elements");
                }
                else
                {
                    throw new Exception("Need more input elements");
                }
            }

            if (elements[1].Length != 1)
            {
                throw new Exception("Invalid operator");
            }

            if (!operations.Contains(elements[1][0]))
            {
                throw new Exception("Invalid operator");
            }

            var a = Convert.ToDouble(elements[0]);
            char operation = elements[1][0];
            var b = Convert.ToDouble(elements[2]);

            switch (operation)
            {
                case '+':
                    Console.WriteLine(a + b);
                    break;
                case '-':
                    Console.WriteLine(a - b);
                    break;
                case '*':
                    Console.WriteLine(a * b);
                    break;
                case '/':
                    Console.WriteLine(a / b);
                    break;
                case '^':
                    Console.WriteLine(Math.Pow(a, b));
                    break;
                default:
                    throw new Exception("Somethig is wrong");
            }

            Console.ReadKey();
        }

        //Заполнить 2 матрицы NxN случайными числами до 100. Сложить две матрицы, вывести на консоль.
        static void Task4()
        {
            const int N = 11;
            var rnd = new Random();

            var matrix1 = new int[N, N];
            var matrix2 = new int[N, N];

            Console.WriteLine("Matrix 1:");
            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < N; j++)
                {
                    matrix1[i, j] = rnd.Next(100);
                    matrix2[i, j] = rnd.Next(100);
                    Console.Write(matrix1[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\nMatrix 2:");
            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < N; j++)
                {
                    Console.Write(matrix2[i, j] + " ");
                }
                Console.WriteLine();
            }

            var resMatrix = new int[N, N];
            Console.WriteLine("\nMatrix 1 + Matrix 2:");
            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < N; j++)
                {
                    resMatrix[i, j] = matrix1[i, j] + matrix2[i, j];
                    Console.Write(resMatrix[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }

        //Заполнить матрицу NxN случайными числами до 100. Найти минимум в каждой строке.
        static void Task5()
        {
            const int N = 5;
            var rnd = new Random();
            var matrix = new int[N, N];
            var minimums = new int[N];

            for (int i = 0; i < N; i++)
            {
                minimums[i] = 101;
                for (int j = 0; j < N; j++)
                {
                    matrix[i, j] = rnd.Next(100);
                    Console.Write(matrix[i, j] + " ");
                    if (minimums[i] > matrix[i, j])
                    {
                        minimums[i] = matrix[i, j];
                    }
                }
                Console.WriteLine();
            }

            for (var i = 0; i < N; i++)
            {
                Console.WriteLine("В строке {0} минимальное число = {1}", i + 1, minimums[i]);
            }

            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            const string LINE = "----------------------------";
            
            Console.WriteLine("Ввести с консоли N чисел, найти их сумму");
            Task1();
            Console.WriteLine(LINE);

            Console.WriteLine("Возвести число X в степень N");
            Task2();
            Console.WriteLine(LINE);

            Console.WriteLine("Калькулятор. Вводится левый операнд, оператор, правый оператор. Выводится результат. Предусмотреть обработку ошибок.");
            try
            {
                Task3();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString() + e.Message);
                Console.ReadKey();
            }
            Console.WriteLine(LINE);
            

            Console.WriteLine("Заполнить 2 матрицы NxN случайными числами до 100. Сложить две матрицы, вывести на консоль.");
            Task4();
            Console.WriteLine(LINE);


            Console.WriteLine("Заполнить матрицу NxN случайными числами до 100. Найти минимум в каждой строке.");
            Task5();
            Console.WriteLine(LINE);
        }
    }
}
